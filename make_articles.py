import json

from compiler.compile_article import compile_article

if __name__ == "__main__":
    with open("articles.json") as articles_list_file:
        articles_list = json.load(articles_list_file)
        # Make all the articles
        for article in articles_list:
            # Read the article file, convert to HTML
            with open("{}/index.article".format(article["path"])) as article_file:
                html = compile_article(article_file.read())
            # Write HTML to new file
            with open("{}/index.html".format(article["path"]), "w") as output_file:
                output_file.write(html)

        # Make the home page
        with open("home.html") as home_template_file:
            HOME_TEMPLATE = home_template_file.read()
        with open("templates/head.html") as head_template_file:
            HEAD_TEMPLATE = head_template_file.read()
        with open("templates/foot.html") as foot_template_file:
            FOOT_TEMPLATE = foot_template_file.read()
        list_html = ""
        for article in articles_list:
            list_html += "<li><a href='{path}'>{title}</a></li>".format(path=article["path"], title=article["title"])
        html = "{}{}{}".format(
            HEAD_TEMPLATE.format(path_to_root="", title="Home", extras=""),
            HOME_TEMPLATE.format(list_html),
            FOOT_TEMPLATE
        )
        with open("index.html", "w") as output_file:
            output_file.write(html)
