from collections import namedtuple

ParseResult = namedtuple("ParseResult", ["state", "value"])


class ParseState(object):
    def __init__(self, tokens, pos=0):
        self.tokens = tokens
        self.pos = pos

    def __str__(self):
        return "Pos: {}, Peek: {}".format(self.pos, self.peek())

    def at_end(self):
        return self.pos == len(self.tokens)

    def take(self):
        if self.at_end():
            return None
        next = ParseState(self.tokens, self.pos + 1)
        token = self.tokens[self.pos]
        return ParseResult(next, token)

    def expect(self, expected_tag):
        result = self.take()
        if result is None or result.value.tag != expected_tag:
            return None
        return result

    def peek(self):
        return self.tokens[self.pos].tag if not self.at_end() else None
