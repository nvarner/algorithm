from compiler.ActionParser import ActionParser
from compiler.ArticleLexer import ArticleLexer
from compiler.ArticleParser import ArticleParser
from compiler.ParseState import ParseState


def compile_article(source):
    lexer = ArticleLexer()
    tokens = lexer.lex(source)

    state = ParseState(tokens)
    parser = ArticleParser(state)
    actions = parser.parse()

    action_parser = ActionParser(actions)
    resulting_html = action_parser.parse()

    return resulting_html


if __name__ == "__main__":
    import sys

    with open(sys.argv[1]) as file:
        print(compile_article(file.read()))
