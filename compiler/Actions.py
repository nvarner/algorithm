from collections import namedtuple

SetVariable = namedtuple("SetVariable", ["variable", "value"])
Heading = namedtuple("Heading", ["level", "text"])
Paragraph = namedtuple("Paragraph", ["text"])
LinkList = namedtuple("LinkList", ["items"])
List = namedtuple("List", ["items"])
Demo = namedtuple("Demo", ["id"])
Code = namedtuple("Code", ["lang", "file"])
