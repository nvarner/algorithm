with open("templates/head.html") as head_file:
    HEAD_TEMPLATE = head_file.read()

FOOT_TEMPLATE = "</body></html>"

TOC_HEAD_FOOT_TEMPLATE = "<h3>Contents</h3><nav><ul>{}</ul></nav>"

TOC_ITEM_TEMPLATE = "<li><a href='#{}'>{}</a></li>"


class ActionParser(object):
    def __init__(self, actions):
        self.actions = actions
        self.title = ""
        self.extras = ""
        self.headings = []
        self.heading_index = 1

    def parse(self):
        res = ""
        for action in self.actions:
            action_type = type(action).__name__
            if action_type == "SetVariable":
                variable = action.variable
                if variable == "$title":
                    self.title = action.value
                elif variable == "$extras":
                    self.extras = action.value
            elif action_type == "Heading":
                if action.level == 1:
                    text = "{}. {}".format(self.heading_index, action.text)
                    heading_id = self.heading_index
                    res += "<h{0} id='{2}'>{1}</h{0}>".format(action.level + 2, text, heading_id)
                    self.headings.append(heading_id)
                    self.headings.append(text)
                    self.heading_index += 1
                else:
                    res += "<h{0}>{1}</h{0}>".format(action.level + 2, action.text)
            elif action_type == "Paragraph":
                res += "<p>{}</p>".format(action.text)
            elif action_type == "LinkList":
                items = action.items
                links = []
                texts = []
                for item in items:
                    broken = item.split(" ")
                    links.append(broken[0])
                    texts.append(" ".join(broken[1:]))
                res += "<ul>"
                res += ("<li><a href='{}'>{}</a></li>" * len(action.items)).format(*links, *texts)
                res += "</ul>"
            elif action_type == "List":
                res += "<ul>"
                res += ("<li>{}</li>" * len(action.items)).format(*action.items)
                res += "</ul>"
            elif action_type == "Demo":
                res += "<div id='{}' class='physics'></div>".format(action.id)
            elif action_type == "Code":
                with open(action.file) as file:
                    file_text = file.read()
                res += "<code><textarea data-lang='{}'>{}</textarea></code>".format(action.lang, file_text)

        head = HEAD_TEMPLATE.format(path_to_root="../", title=self.title, extras=self.extras)
        foot = FOOT_TEMPLATE
        toc_items = (TOC_ITEM_TEMPLATE * int(len(self.headings) / 2)).format(*self.headings)
        toc = TOC_HEAD_FOOT_TEMPLATE.format(toc_items)

        php = "{}{}{}{}".format(head, toc, res, foot)

        return php
