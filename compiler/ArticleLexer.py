import re

from compiler.article_tokens import *


class ArticleLexer(object):
    def __init__(self):
        self.token_regex = [
            Token(r"\$[^\s]+\$", STRING),  # Math equations, anything but whitespace between two $
            Token(r"\$[a-zA-Z0-9]*", VARIABLE),  # Variable, alphanumeric prefixed by a $
            Token(r"=", EQUAL_SIGN),
            Token(r"#{1,4}", HEADING),  # 1-4 '#'s
            Token(r"[ \t]+", WHITESPACE),  # Space or tab
            Token(r"\n+", LINE_BREAK),  # Line break(s)
            Token(r"->", LINK_LIST_ITEM),
            Token(r"-", LIST_ITEM),
            Token(r"<{", START_CODE),
            Token(r"}>", END_CODE),
            Token(r"{", START_DEMO),
            Token(r"}", END_DEMO),
            Token(r"[^\n{}]+", STRING)  # String with no linebreaks or "{}"
        ]
        self.compile_regex()

    def compile_regex(self):
        self.token_regex[:] = [Token(re.compile(token_regex.text), token_regex.tag) for token_regex in self.token_regex]

    def lex(self, source_string):
        pos = 0
        tokens = []
        while pos < len(source_string):
            match = None
            for token_regex in self.token_regex:
                match = token_regex.text.match(source_string, pos)
                if match:
                    token_text = match.group(0)
                    if token_regex.tag:  # Just ignore it if the token type is None
                        token = Token(token_text, token_regex.tag)
                        tokens.append(token)
                    break
            if match is None:
                print("Couldn't find match for \"{}\".".format(source_string[pos]))
                return
            else:
                pos = match.end(0)
        return tokens
