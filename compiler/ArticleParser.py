from compiler import Actions
from compiler.ParseState import ParseState
from compiler.article_tokens import *


class ArticleParser(object):
    def __init__(self, state: ParseState):
        self.state = state
        self.actions = []

    def parse(self):
        while not self.state.at_end():
            self.state = ArticleParser.ignore_whitespace_linebreak(self.state)
            if self.parse_variable():
                continue
            if self.parse_heading():
                continue
            if self.parse_paragraph():
                continue
            if self.parse_link_list():
                continue
            if self.parse_list():
                continue
            if self.parse_demo():
                continue
            if self.parse_code():
                continue
            print("Unable to parse {}.".format(self.state))
            break
        return self.actions

    @staticmethod
    def ignore_whitespace(state):
        while True:
            if state.peek() != WHITESPACE:
                break
            state = state.take().state
        return state

    @staticmethod
    def ignore_whitespace_linebreak(state):
        while True:
            any_good = False
            res = state.expect(WHITESPACE)
            if res is not None:
                any_good = True
            res = state.expect(LINE_BREAK)
            if res is not None:
                any_good = True

            if not any_good:
                break
            state = state.take().state
        return state

    @staticmethod
    def get_string(state):
        res = state.expect(STRING)
        if res is None:
            return None, None
        text = res.value.text
        while True:
            state = res.state

            any_good = False
            res2 = state.expect(STRING)
            if res2 is not None:
                any_good = True
            res2 = state.expect(WHITESPACE)
            if res2 is not None:
                any_good = True
            res2 = state.expect(EQUAL_SIGN)
            if res2 is not None:
                any_good = True
            if not any_good:
                return res, text

            res = state.take()
            text += res.value.text

    def parse_variable(self):
        state = self.state

        # Expect a variable
        res = state.expect(VARIABLE)
        if res is None:
            return None
        variable = res.value.text

        state = res.state
        state = ArticleParser.ignore_whitespace(state)

        # Expect an equal sign
        res = state.expect(EQUAL_SIGN)
        if res is None:
            return None

        state = res.state
        state = ArticleParser.ignore_whitespace(state)

        # Expect a string
        res, text = ArticleParser.get_string(state)
        if res is None:
            return None

        self.state = res.state
        self.actions.append(Actions.SetVariable(variable, text))
        return True

    def parse_heading(self):
        state = self.state

        # Expect heading
        res = state.expect(HEADING)
        if res is None:
            return None
        heading = res.value.text

        state = res.state
        state = ArticleParser.ignore_whitespace(state)

        # Expect a string
        res, text = ArticleParser.get_string(state)
        if res is None:
            return None

        self.state = res.state
        self.actions.append(Actions.Heading(len(heading), text))
        return True

    def parse_paragraph(self):
        state = self.state

        # Expect a string
        res, text = ArticleParser.get_string(state)
        if res is None:
            return None

        while True:
            # Ignore linebreaks
            state = ArticleParser.ignore_whitespace_linebreak(res.state)

            # Expect a string
            res2, text2 = ArticleParser.get_string(state)
            if res2 is None:
                break
            res = res2
            text += " {}".format(text2)
        self.state = state
        self.actions.append(Actions.Paragraph(text))
        return True

    def parse_link_list(self):
        state = self.state

        # Expect link list item
        res = state.expect(LINK_LIST_ITEM)
        if res is None:
            return None
        state = res.state

        state = ArticleParser.ignore_whitespace(state)

        # Expect a string
        res, text = ArticleParser.get_string(state)
        if res is None:
            return None
        state = res.state
        items = [text]

        while True:
            state = ArticleParser.ignore_whitespace_linebreak(state)

            # Expect link list item
            res2 = state.expect(LINK_LIST_ITEM)
            if res2 is None:
                break
            state2 = res2.state

            state2 = ArticleParser.ignore_whitespace(state2)

            # Expect a string
            res2, text = ArticleParser.get_string(state2)
            if res2 is None:
                return None  # All list items must be followed by a string
            state = res2.state
            items.append(text)

        self.state = state
        self.actions.append(Actions.LinkList(items))
        return True

    def parse_list(self):
        state = self.state

        # Expect list item
        res = state.expect(LIST_ITEM)
        if res is None:
            return None
        state = res.state

        state = ArticleParser.ignore_whitespace(state)

        # Expect a string
        res, text = ArticleParser.get_string(state)
        if res is None:
            return None
        state = res.state
        items = [text]

        while True:
            state = ArticleParser.ignore_whitespace_linebreak(state)

            # Expect list item
            res2 = state.expect(LIST_ITEM)
            if res2 is None:
                break
            state2 = res2.state

            state2 = ArticleParser.ignore_whitespace(state2)

            # Expect a string
            res2, text = ArticleParser.get_string(state2)
            if res2 is None:
                return None  # All list items must be followed by a string
            state = res2.state
            items.append(text)

        self.state = state
        self.actions.append(Actions.List(items))
        return True

    def parse_demo(self):
        state = self.state

        # Expect start demo
        res = state.expect(START_DEMO)
        if res is None:
            return None
        state = res.state

        # Expect string
        res, text = ArticleParser.get_string(state)
        if res is None:
            return None
        demo_id = text
        state = res.state

        # Expect end demo
        res = state.expect(END_DEMO)
        if res is None:
            return None

        self.state = res.state
        self.actions.append(Actions.Demo(demo_id))
        return True

    def parse_code(self):
        state = self.state

        # Expect start code
        res = state.expect(START_CODE)
        if res is None:
            return None
        state = res.state

        # Expect string
        res, text = ArticleParser.get_string(state)
        if res is None:
            return None
        lang_file = text
        state = res.state

        # Expect end code
        res = state.expect(END_CODE)
        if res is None:
            return None

        self.state = res.state

        lang, file = lang_file.split(" ")
        self.actions.append(Actions.Code(lang, file))
        return True
