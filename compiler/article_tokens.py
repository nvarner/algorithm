from collections import namedtuple

VARIABLE = "VARIABLE"
STRING = "STRING"
EQUAL_SIGN = "EQUAL_SIGN"
HEADING = "HEADING"
LINK_LIST_ITEM = "LINK_LIST_ITEM"
LIST_ITEM = "LIST_ITEM"
WHITESPACE = "WHITESPACE"
LINE_BREAK = "LINE_BREAK"
START_DEMO = "START_DEMO"
END_DEMO = "END_DEMO"
START_CODE = "START_CODE"
END_CODE = "END_CODE"

Token = namedtuple("Token", ["text", "tag"])
