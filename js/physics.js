const Engine = Matter.Engine,
    Render = Matter.Render,
    World = Matter.World,
    Bodies = Matter.Bodies,
    Runner = Matter.Runner,
    Composite = Matter.Composite,
    Composites = Matter.Composites,
    Constraint = Matter.Constraint,
    Body = Matter.Body,
    Events = Matter.Events;

class OutputPane {
    constructor(element, columnNames) {
        this.element = element;
        this.columnNames = columnNames;

        this.list = document.createElement("ul");
        this.list.classList.add("output-list");
        this.columnNames.forEach((column) => {
            let li = document.createElement("li");
            let data = document.createElement("span");
            data.classList.add("data");
            let text = document.createTextNode(column + ": ");
            li.appendChild(text);
            li.appendChild(data);
            this.list.appendChild(li);
        });

        this.element.appendChild(this.list);
    }

    setValues(values) {
        this.clearDataSpans();

        this.list.querySelectorAll(".data").forEach((dataSpan, i) => {
            let text = document.createTextNode(values[i]);
            dataSpan.appendChild(text);
        });
    }

    clearDataSpans() {
        let dataSpans = this.list.querySelectorAll(".data");
        dataSpans.forEach((dataSpan) => {
            while (dataSpan.firstChild) {
                dataSpan.removeChild(dataSpan.firstChild);
            }
        });
    }
}

class Control {
    constructor(id, label) {
        this.id = id;
        
        this.label = label;
        this.labelElement = document.createElement("label");
        this.labelElement.setAttribute("for", this.id);
        let labelText = document.createTextNode(this.label);
        this.labelElement.appendChild(labelText);

        this.element = document.createElement("div");
        this.element.setAttribute("id", this.id);
        this.element.appendChild(this.labelElement);
    }

    get value() {}
    set value(value) {}
}

class Number extends Control {
    constructor(id, label, min, max, step, value) {
        super(id, label);

        this.slider = document.createElement("input");
        this.slider.setAttribute("type", "range");
        this.slider.setAttribute("min", min);
        this.slider.setAttribute("max", max);
        this.slider.setAttribute("step", step);
        this.slider.setAttribute("value", value);
        this.element.appendChild(this.slider);

        this.numberInput = document.createElement("input");
        this.numberInput.setAttribute("type", "number");
        this.numberInput.setAttribute("min", min);
        this.numberInput.setAttribute("max", max);
        this.numberInput.setAttribute("step", step);
        this.numberInput.setAttribute("value", value);
        this.element.appendChild(this.numberInput);

        this.slider.addEventListener("input", () => {
            this.value = this.slider.value;
        });
        this.numberInput.addEventListener("input", () => {
            this.value = this.numberInput.value;
        });
    }

    get value() {
        return this.numberInput.value;
    }
    set value(value) {
        this.numberInput.value = value;
        this.slider.value = value;
    }
}

class InteractivePane {
    constructor(element) {
        this.element = element;
        this.controls = {};
        this.headings = {};
    }

    addControl(control) {
        this.controls[control.id] = control;
        this.element.prepend(control.element);
    }

    removeControl(id) {
        let control = this.controls[id];
        this.element.removeChild(control.element);
        this.controls.remove(this.controls.indexOf(control));
    }

    static createControlElement(type, options) {
        let controlElement;
        switch (type) {
            case "slider":
                controlElement = document.createElement("input");
                controlElement.setAttribute("type", "range");
                controlElement.setAttribute("min", options["min"]);
                controlElement.setAttribute("max", options["max"]);
                controlElement.setAttribute("step", options["step"]);
                controlElement.setAttribute("value", options["default"]);
                break;
            default:
                controlElement = document.createElement("input");
                break;
            // TODO: Add in more control types. Nice list available at
            // https://developer.mozilla.org/en-US/docs/Learn/HTML/Forms/The_native_form_widgets
        }

        controlElement.classList.add("control-element");

        return controlElement;
    }

    addHeading(id, text) {
        let heading = document.createElement("h3");
        let headingText = document.createTextNode(text);
        heading.appendChild(headingText);
        this.headings[id] = heading;
        this.element.prepend(heading);
    }

    removeHeading(id) {
        let heading = this.headings[id];
        this.element.removeChild(heading);
        this.headings.remove(this.headings.indexOf(heading));
    }
}

class PhysicsEnvironment {
    /**
     * @param {Object} element - The element to turn into the physics environment
     */
    constructor(element) {
        this.element = element;

        this.controlBar = document.createElement("div");
        this.playButton = document.createElement("button");
        this.pauseButton = document.createElement("button");
        this.resetButton = document.createElement("button");
        this.container = document.createElement("div");
        this.interactive = document.createElement("div");
        this.output = document.createElement("div");

        this.controlBar.classList.add("control-bar");
        this.playButton.classList.add("play-button");
        this.pauseButton.classList.add("pause-button");
        this.resetButton.classList.add("reset-button");
        this.container.classList.add("container");
        this.interactive.classList.add("interactive");
        this.output.classList.add("output");

        this.controlBar.appendChild(this.playButton);
        this.controlBar.appendChild(this.pauseButton);
        this.controlBar.appendChild(this.resetButton);
        this.element.appendChild(this.controlBar);
        this.container.appendChild(this.output);
        this.container.appendChild(this.interactive);
        this.element.appendChild(this.container);

        this.playButton.addEventListener("click", () => {
            this.play();
        });
        this.pauseButton.addEventListener("click", () => {
            this.pause();
        });
        this.resetButton.addEventListener("click", () => {
            this.reset();
        });

        this.currentState = "reset";

        this.engine = Engine.create();
        this.render = Render.create({
            element: this.element.querySelector(".container"),
            engine: this.engine
        });
        Render.run(this.render);

        this.runner = Runner.create();
        this.runner.isFixed = true;
        Runner.run(this.runner, this.engine);
        this.runner.enabled = false;

        this.reset();
    }

    onFinish(nextUp) {
        if (this.currentState === nextUp) {
            // No actual change
            return;
        }

        if (this.currentState === "play") {
            this.onEndPlay();
        } else if (this.currentState === "pause") {
            this.onEndPause();
        } else if (this.currentState === "reset") {
            this.onEndReset();
        }

        this.currentState = nextUp;
    }

    play() {
        this.onFinish("play");
        this.onStartPlay();
        this.runner.enabled = true;
    }
    pause() {
        this.onFinish("pause");
        this.onStartPause();
        this.runner.enabled = false;
    }
    reset() {
        this.onFinish("reset");
        // Clear everything first
        World.clear(this.engine.world);
        Engine.clear(this.engine);

        this.onStartReset();

        this.pause();
    }

    onStartPlay() {}
    onStartPause() {}
    onStartReset() {}

    onEndPlay() {}
    onEndPause() {}
    onEndReset() {}
}
