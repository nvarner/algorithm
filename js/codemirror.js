const codeTextareas = document.querySelectorAll("code > textarea");
codeTextareas.forEach(textarea => {
    CodeMirror.fromTextArea(textarea, {
        viewportMargin: Infinity,
        mode: textarea.getAttribute("data-lang"),
        readOnly: true
    });
});
