import time


class PID:
    """This is a basic PID controller implementation."""
    def __init__(self, kp, ki, kd, setpoint):
        """
        :param kp: The proportional weight, obtained through tuning
        :param ki: The integral weight, obtained through tuning
        :param kd: The derivative weight, obtained through tuning
        :param setpoint: The target for the measured value
        """
        # Weights
        self.kp = kp
        self.ki = ki
        self.kd = kd

        # Target
        self.setpoint = setpoint

        # Integral value
        self.integral = 0  # There is no error at the start, so set it to zero

        # Save some values for the derivative
        self.last_error = 0
        # last_time is in seconds, but any time unit should work, if it is consistent, but make sure that each iteration
        # will be far enough apart in time that delta_time isn't zero (eg. no integer seconds/minutes)
        self.last_time = time.time()

    def p(self, error):
        """
        Calculate the proportional component of the PID controller based on the error. The proportional component is
        the error or difference between the setpoint and measured value. Its contribution to the controller is to do
        most of the "work" of minimizing the error. The proportional component alone would minimize the error very fast,
        but because of momentum would end up overshooting and oscillating. The PID controller is designed to prevent
        this oscillation.
        :param error: The difference between the setpoint and measured value
        :return: The proportional component
        """
        return self.kp * error

    def i(self, error, delta_time):
        """
        Calculate the integral component of the PID controller based on the error and delta time. The integral component
        is the integral of a graph of the error, or the sum of all errors times the time since last iteration. Its
        contribution to the controller is to reduce steady state error, where the measured value is just above or below
        the setpoint and is changing slowly. The integral grows quickly as the small error adds up with time to
        counteract this steady state error.
        :param error: The difference between the setpoint and measured value
        :param delta_time: The difference between this iteration's time and last iteration's time
        :return: The integral component
        """
        # Add the new area under the curve
        self.integral += (error * delta_time)
        # Don't save the multiplication by ki back in integral
        return self.ki * self.integral

    def d(self, error, delta_time):
        """
        Calculate the derivative component of the PID controller based on the error and delta time.  The derivative
        component is the derivative of the error. Its contribution to the controller is to prevent overshoot by trying
        to flatten out the error graph. It can "predict" the oscillations to stop or at least shorten them.
        :param error: The difference between the setpoint and measured value
        :param delta_time: The difference between this iteration's time and last iteration's time
        :return: The derivative component
        """
        # Calculate the delta
        delta_error = error - self.last_error

        # Update the last error
        self.last_error = error

        return self.kd * (delta_error / delta_time)

    def pid(self, measured_value):
        """
        Calculates the PID controller's output for this iteration.
        :param measured_value: The value measured this iteration
        :return: The PID controller's output
        """
        error = self.setpoint - measured_value
        current_time = time.time()  # Same time unit as above
        delta_time = current_time - self.last_time
        self.last_time = current_time

        return self.p(error) + self.i(error, delta_time) + self.d(error, delta_time)

# Usage
my_pid = PID(
    0.1, 0.2, 0.3,  # The weights are obtained through tuning, these aren't real values and won't work for you
    5  # The target
)

for i in range(1000):
    measured_value = get_value()  # This could be reading a gyroscope, for example
    pid_result = my_pid.pid(measured_value)
    update_something(pid_result)  # This could be moving a motor, for example
