/**
 * A basic PID controller implementation
 * @param {Number} kp - The proportional weight, obtained through tuning
 * @param {Number} ki - The integral weight, obtained through tuning
 * @param {Number} kd - The derivative weight, obtained through tuning
 * @param {Number} setpoint - The target for the measured value
 */
class PID {
    constructor(kp, ki, kd, setpoint) {
        // Weights
        this.kp = kp;
        this.ki = ki;
        this.kd = kd;

        // Target
        this.setpoint = setpoint;

        // Integral value
        this.integral = 0;

        // Save some values for the derivative
        this.lastError = 0;
    }

    /**
     * Calculate the proportional component of the PID controller based on the error. The proportional component is
     * the error or difference between the setpoint and measured value. Its contribution to the controller is to do
     * most of the "work" of minimizing the error. The proportional component alone would minimize the error very fast,
     * but because of momentum would end up overshooting and oscillating. The PID controller is designed to prevent
     * this oscillation.
     * @param {Number} error - The difference between the setpoint and measured value
     * @returns {Number} The proportional component
     */
    p(error) {
        return this.kp * error;
    }

    /**
     * Calculate the integral component of the PID controller based on the error and delta time. The integral component
     * is the integral of a graph of the error, or the sum of all errors times the time since last iteration. Its
     * contribution to the controller is to reduce steady state error, where the measured value is just above or below
     * the setpoint and is changing slowly. The integral grows quickly as the small error adds up with time to
     * counteract this steady state error.
     * @param {Number} error - The difference between the setpoint and measured value
     * @returns {Number} The integral component
     */
    i(error) {
        // Add the new area under the curve
        this.integral += error;
        // Don't save the multiplication by ki back in integral
        return this.ki * this.integral;
    }

    /**
     * Calculate the derivative component of the PID controller based on the error and delta time.  The derivative
     * component is the derivative of the error. Its contribution to the controller is to prevent overshoot by trying
     * to flatten out the error graph. It can "predict" the oscillations to stop or at least shorten them.
     * @param {Number} error - The difference between the setpoint and measured value
     * @returns {Number} The derivative component
     */
    d(error) {
        // Calculate the delta
        let deltaError = error - this.lastError;

        // Update the last error
        this.lastError = error;

        return this.kd * deltaError;
    }

    /**
     * Calculates the PID controller's output for this iteration.
     * @param {Number} measuredValue - The value measured this iteration
     * @returns {Number} The PID controller's output
     */
    pid(measuredValue) {
        let error = this.setpoint - measuredValue;

        return this.p(error) + this.i(error) + this.d(error);
    }

    resetIntegration() {
        this.integral = 0;
    }
}

class PidBall extends PhysicsEnvironment {
    constructor() {
        super(document.querySelector("#ball"));

        this.kp = 0.00004;
        this.ki = 0.0000015;
        this.kd = 0.002;

        // Set up interactive
        this.interactivePane = new InteractivePane(this.interactive, "Interactive");
        let controlP = new Number("p", "P", 0, this.kp * 2, this.kp / 5, this.kp);
        let controlI = new Number("i", "I", 0, this.ki * 2, this.ki / 5, this.ki);
        let controlD = new Number("d", "D", 0, this.kd * 2, this.kd / 5, this.kd);
        this.interactivePane.addControl(controlD);
        this.interactivePane.addControl(controlI);
        this.interactivePane.addControl(controlP);
        this.interactivePane.addHeading("interactive", "PID");

        // Set up output
        this.outputPane = new OutputPane(this.output, ["PID", "P", "I", "D"], this.chart);

        this.pidController = new PID(this.kp, this.ki, this.kd, 200);
        Events.on(this.engine, "beforeUpdate", () => {
            let e = this.pidController.setpoint - this.ball.position.y;
            let p = this.pidController.p(e);
            let i = this.pidController.i(e);
            let d = this.pidController.d(e);
            let pid = p + i + d;

            Body.applyForce(this.ball, {x: 400, y: this.ball.position.y - 50}, {x: 0, y: pid});
            this.outputPane.setValues([pid, p, i, d]);

            this.pidController.kp = controlP.value;
            this.pidController.ki = controlI.value;
            this.pidController.kd = controlD.value;
        });
    }

    onStartReset() {        
        this.ball = Bodies.circle(400, 600, 40);
        this.pointer1 = Bodies.rectangle(350, 200, 10, 2, {isStatic: true});
        this.pointer2 = Bodies.rectangle(450, 200, 10, 2, {isStatic: true});

        World.add(this.engine.world, [this.ball, this.pointer1, this.pointer2]);
    }

    onEndReset() {
        if (this.pidController) {
            this.pidController.resetIntegration();
        }
    }
}

const physicsEnv = new PidBall();