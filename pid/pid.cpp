#include "pid.h"

PID::PID(double kp, double ki, double kd, double setpoint) {
    m_kp = kp;
    m_ki = ki;
    m_kd = kd;
    m_setpoint = setpoint;
    m_lastTime = PID::now();
    m_integral = 0;
    m_lastError = 0;
}

long int PID::now() {
    return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}

double PID::pid(double measuredValue) {
    double error = m_setpoint - measuredValue;
    long int now = PID::now();
    long int deltaTime = now - m_lastTime;
    if (deltaTime == 0) deltaTime++;
    m_lastTime = now;
    return p(error) + i(error, deltaTime) + d(error, deltaTime);
}

double PID::p(double error) {
    return error * m_kp;
}

double PID::i(double error, long int deltaTime) {
    m_integral += error * deltaTime;
    return m_ki * m_integral;
}

double PID::d(double error, long int deltaTime) {
    double deltaError = error - m_lastError;
    m_lastError = error;
    double derivative = deltaError / deltaTime;
    return m_kd * derivative;
}
