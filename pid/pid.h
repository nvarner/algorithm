#ifndef PID_H
#define PID_H

#include <chrono>
#include <stdio.h>

class PID {
private:
    // Weights
    double m_kp;
    double m_ki;
    double m_kd;
    // Setpoint
    double m_setpoint;
    // Save some things for I and D
    double m_integral;
    double m_lastError;
    long int m_lastTime;
public:
    /**
     * @param kp The proportional weight, obtained through tuning
     * @param ki The integral weight, obtained through tuning
     * @param kd The derivative weight, obtained through tuning
     * @param setpoint The target for the measured value
     */
    PID(double kp, double ki, double kd, double setpoint);
    /**
     * @brief now Method to get the current time in ms
     * @return The current time in ms
     */
    static long int now();
    /**
     * @brief pid Calculates the PID controller's output for this iteration.
     * @param measuredValue The value measured this iteration
     * @return The PID controller's output
     */
    double pid(double measuredValue);
    /**
     * @brief p Calculate the proportional component of the PID controller based on the error. The proportional component is
     * the error or difference between the setpoint and measured value. Its contribution to the controller is to do
     * most of the "work" of minimizing the error. The proportional component alone would minimize the error very fast,
     * but because of momentum would end up overshooting and oscillating. The PID controller is designed to prevent
     * this oscillation.
     * @param error The difference between the setpoint and measured value
     * @return The proportional component
     */
    double p(double error);
    /**
     * @brief i Calculate the integral component of the PID controller based on the error and delta time. The integral component
     * is the integral of a graph of the error, or the sum of all errors times the time since last iteration. Its
     * contribution to the controller is to reduce steady state error, where the measured value is just above or below
     * the setpoint and is changing slowly. The integral grows quickly as the small error adds up with time to
     * counteract this steady state error.
     * @param error The difference between the setpoint and measured value
     * @param deltaTime The difference between this iteration's time and last iteration's time
     * @return The integral component
     */
    double i(double error, long int deltaTime);
    /**
     * @brief d Calculate the derivative component of the PID controller based on the error and delta time.  The derivative
     * component is the derivative of the error. Its contribution to the controller is to prevent overshoot by trying
     * to flatten out the error graph. It can "predict" the oscillations to stop or at least shorten them.
     * @param error The difference between the setpoint and measured value
     * @param deltaTime The difference between this iteration's time and last iteration's time
     * @return The derivative component
     */
    double d(double error, long int deltaTime);
};

#endif // PID_H
