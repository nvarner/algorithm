/**
 * A basic PID controller implementation
 * @param {Number} kp - The proportional weight, obtained through tuning
 * @param {Number} ki - The integral weight, obtained through tuning
 * @param {Number} kd - The derivative weight, obtained through tuning
 * @param {Number} setpoint - The target for the measured value
 */
function PID(kp, ki, kd, setpoint) {
    // Weights
    this.kp = kp;
    this.ki = ki;
    this.kd = kd;

    // Target
    this.setpoint = setpoint;

    // Integral value
    this.integral = 0;

    // Save some values for the derivative
    this.lastError = 0;
    // lastTime is in milliseconds, but any time unit should work, if it is consistent, but make sure that each
    // iteration will be far enough apart in time that deltaTime isn't zero (eg. no integer seconds/minutes)
    this.lastTime = Date.now();

    /**
     * Calculate the proportional component of the PID controller based on the error. The proportional component is
     * the error or difference between the setpoint and measured value. Its contribution to the controller is to do
     * most of the "work" of minimizing the error. The proportional component alone would minimize the error very fast,
     * but because of momentum would end up overshooting and oscillating. The PID controller is designed to prevent
     * this oscillation.
     * @param {Number} error - The difference between the setpoint and measured value
     * @returns {Number} The proportional component
     */
    this.p = function(error) {
        return this.kp * error;
    }

    /**
     * Calculate the integral component of the PID controller based on the error and delta time. The integral component
     * is the integral of a graph of the error, or the sum of all errors times the time since last iteration. Its
     * contribution to the controller is to reduce steady state error, where the measured value is just above or below
     * the setpoint and is changing slowly. The integral grows quickly as the small error adds up with time to
     * counteract this steady state error.
     * @param {Number} error - The difference between the setpoint and measured value
     * @param {Number} deltaTime - The difference between this iteration's time and last iteration's time
     * @returns {Number} The integral component
     */
    this.i = function(error, deltaTime) {
        // Add the new area under the curve
        this.integral += (error * deltaTime);
        // Don't save the multiplication by ki back in integral
        return this.ki * this.integral;
    }

    /**
     * Calculate the derivative component of the PID controller based on the error and delta time.  The derivative
     * component is the derivative of the error. Its contribution to the controller is to prevent overshoot by trying
     * to flatten out the error graph. It can "predict" the oscillations to stop or at least shorten them.
     * @param {Number} error - The difference between the setpoint and measured value
     * @param {Number} deltaTime - The difference between this iteration's time and last iteration's time
     * @returns {Number} The derivative component
     */
    this.d = function(error, deltaTime) {
        // Calculate the delta
        var deltaError = error - this.lastError;

        // Update the last error
        this.lastError = error;

        return this.kd * (deltaError / deltaTime);
    }

    /**
     * Calculates the PID controller's output for this iteration.
     * @param {Number} measuredValue - The value measured this iteration
     * @returns {Number} The PID controller's output
     */
    this.pid = function(measuredValue) {
        var error = this.setpoint - measuredValue;
        var currentTime = Date.now(); // Same time unit as above
        var deltaTime = currentTime - this.lastTime;
        console.log("c: " + currentTime);
        console.log("d: " + deltaTime);
        console.log("l: " + this.lastTime);
        this.lastTime = currentTime;

        return this.p(error) + this.i(error, deltaTime) + this.d(error, deltaTime);
    }
}

// Usage
var myPid = new PID(
    0.1, 0.2, 0.3, // The weights are obtained through tuning, these aren't real values and won't work for you
    5 // The target
);
for (var i = 0; i < 1000; i++) {
    measuredValue = getValue(); // This could be reading a gyroscope, for example
    pidResult = myPid.pid(measuredValue);
    updateSomething(pidResult); // This could be moving a motor, for example
}