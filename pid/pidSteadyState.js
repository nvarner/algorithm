// Trust that the PID class is already defined from pidBall.js

class PidSteadyState extends PhysicsEnvironment {
    constructor() {
        super(document.querySelector("#steady-state"));

        this.kp = 0.00004;
        this.ki = 0;
        this.kd = 0.002;

        // Set up output
        this.outputPane = new OutputPane(this.output, ["Ball Y", "Setpoint"]);

        this.pidController = new PID(this.kp, this.ki, this.kd, 200);
        Events.on(this.engine, "beforeUpdate", () => {
            let ballY = this.ball.position.y;
            let pid = this.pidController.pid(ballY);
            Body.applyForce(this.ball, { x: 400, y: ballY - 50 }, { x: 0, y: pid });
            this.outputPane.setValues([ballY, 200]);
        });
    }

    onStartReset() {        
        this.ball = Bodies.circle(400, 600, 40);
        this.pointer1 = Bodies.rectangle(350, 200, 10, 2, {isStatic: true});
        this.pointer2 = Bodies.rectangle(450, 200, 10, 2, {isStatic: true});

        World.add(this.engine.world, [this.ball, this.pointer1, this.pointer2]);
    }

    onEndReset() {
        if (this.pidController) {
            this.pidController.resetIntegration();
        }
    }
}

const physicsEnvSteadyState = new PidSteadyState();